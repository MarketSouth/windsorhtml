const navOverlay = document.querySelector('.nav-overlay');
const countryOverlay = document.querySelector('.country-overlay');
const searchOverlay = document.querySelector('.search-overlay');
const navIcon = document.querySelector('.nav-icon');
const countryIcon = document.querySelector('.country-icon');

function active() {
  navOverlay.classList.toggle('nav-overlay-active');
  navIcon.classList.toggle('is-active');
}

function countryActive() {
  countryOverlay.classList.toggle('country-overlay-active');
  countryIcon.classList.toggle('is-active');
}

function searchActive() {
  searchOverlay.classList.add('search-overlay-active');
  navOverlay.classList.remove('nav-overlay-active');
  navIcon.classList.remove('is-active');
  countryOverlay.classList.remove('country-overlay-active');
  countryIcon.classList.remove('is-active')
}

function remove() {
  searchOverlay.classList.remove('search-overlay-active');
}


const tabItems = document.querySelectorAll('.tab-item');
const tabContentItems = document.querySelectorAll('.tab-content-item');

// Select tab content item
function selectItem(e) {
	// Remove all show and border classes
	removeBorder();
	removeShow();
	// Add border to current tab item
	this.classList.add('tab-border');
	// Grab content item from DOM
	const tabContentItem = document.querySelector(`#${this.id}-content`);
	// Add show class
	tabContentItem.classList.add('show');
}

// Remove bottom borders from all tab items
function removeBorder() {
	tabItems.forEach(item => {
		item.classList.remove('tab-border');
	});
}

// Remove show class from all content items
function removeShow() {
	tabContentItems.forEach(item => {
		item.classList.remove('show');
	});
}

// Listen for tab item click
tabItems.forEach(item => {
	item.addEventListener('click', selectItem);
});


let tabLine = document.querySelector('.tab-line');
let tabOne = document.querySelector('#tab-1');
let tabTwo = document.querySelector('#tab-2');
let tabThree = document.querySelector('#tab-3');

function toOne() {
  tabLine.style.marginLeft = "0rem";
  tabOne.classList.add('tab-active');
  tabTwo.classList.remove('tab-active');
  tabThree.classList.remove('tab-active');
}
function toTwo() {
  tabLine.style.marginLeft = "3rem";
  tabOne.classList.remove('tab-active');
  tabTwo.classList.add('tab-active');
  tabThree.classList.remove('tab-active');
}
function toThree() {
  tabLine.style.marginLeft = "6rem";
  tabOne.classList.remove('tab-active');
  tabTwo.classList.remove('tab-active');
  tabThree.classList.add('tab-active');
}

// Back to top

(function() {
	var backTop = document.getElementsByClassName('js-back-to-top')[0];
	if( backTop ) {
	  var dataElement = backTop.getAttribute('data-element');
	  var scrollElement = dataElement ? document.querySelector(dataElement) : window;
	  var scrollDuration = parseInt(backTop.getAttribute('data-duration')) || 300, //scroll to top duration
		scrollOffset = parseInt(backTop.getAttribute('data-offset')) || 0, //show back-to-top if scrolling > scrollOffset
		scrolling = false;

	  //detect click on back-to-top link
	  backTop.addEventListener('click', function(event) {
		event.preventDefault();
		if(!window.requestAnimationFrame) {
		  scrollElement.scrollTo(0, 0);
		} else {
		  dataElement ? Util.scrollTo(0, scrollDuration, false, scrollElement) : Util.scrollTo(0, scrollDuration);
		}
		//move the focus to the #top-element - don't break keyboard navigation
		Util.moveFocus(document.getElementById(backTop.getAttribute('href').replace('#', '')));
	  });

	  //listen to the window scroll and update back-to-top visibility
	  checkBackToTop();
	  if (scrollOffset > 0) {
		scrollElement.addEventListener("scroll", function(event) {
		  if( !scrolling ) {
			scrolling = true;
			(!window.requestAnimationFrame) ? setTimeout(function(){checkBackToTop();}, 250) : window.requestAnimationFrame(checkBackToTop);
		  }
		});
	  }

	  function checkBackToTop() {
		var windowTop = scrollElement.scrollTop || document.documentElement.scrollTop;
		if(!dataElement) windowTop = window.scrollY || document.documentElement.scrollTop;
		Util.toggleClass(backTop, 'back-to-top--is-visible', windowTop >= scrollOffset);
		scrolling = false;
	  }
	}
  }());

  // Back to Top

var btn = $('#button');
var btnT = $('#button-2')

$(window).scroll(function() {
  if ($(window).scrollTop() > 300) {
	btn.addClass('appear');
	btnT.removeClass('appear')
  } else {
	btn.removeClass('appear');
	btnT.addClass('appear')
  }
});

btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});


  /* Demo purposes only */
  $(".hover").mouseleave(
    function () {
      $(this).removeClass("hover");
    }
  );




  //Checkbox-Radio


  $('input[type=radio]').bind('touchstart mousedown', function() {
	this.checked = !this.checked
}).bind('click touchend', function(e) {
 	e.preventDefault()
});
